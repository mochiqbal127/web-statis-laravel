<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
</head>
<body>
	<h1>Buat Account Baru</h1>
	<h3>Sign Up Form</h3>
	<form action="{{url('welcome')}}" method="post">
        @csrf
		<p>First Name :</p>
		<input type="text" name="firstname">
		<p>Last Name :</p>
		<input type="text" name="lastname">
		<p>Gender :</p>
		<input type="radio" name="gender">
		<label for="male">Male</label><br>
		<input type="radio" name="gender" >
		<label for="female">Female</label><br>
		<input type="radio" name="gender">
		<label for="other">Other</label>
		<p for="nationality">Nationality :</p>
		<select name="nationality" id="nationality">
			<option value="">Indonesian</option>
			<option value="">Singaporean</option>
			<option value="">Australian</option>
			<option value="">Malaysian</option>
		</select>
		<p>Language Spoken :</p>
		<input type="checkbox">Bahasa Indonesia <br>
		<input type="checkbox">English <br>
		<input type="checkbox">Other <br>
		<p>Bio :</p>
		<textarea name="" id="" cols="30" rows="10"></textarea><br><br>
		<input type="submit" value="Sign Up">
	</form>
</body>
</html>
