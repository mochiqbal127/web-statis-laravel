<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('form');
    }
    public function welcome(){
        return view('welcome');
    }
    public function store(Request $request){
        $fr = $request->firstname;
        $ls = $request->lastname;

        $data = ['fr'=> $fr , 'ls'=>$ls];

        return view('welcome')->with($data);
    }
}
